#include <sstream>      // std::istringstream
#include "myStoi.h"

int myStoi(const std::string s)
{
  int res;
  std::istringstream(s) >> res;
  return res;
}
