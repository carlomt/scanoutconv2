#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
//#pragma link off all extern;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class myDate+;
//#pragma link C++ function operator<< (std::ostream &out, myDate &CmyDate);

#pragma link C++ class myTime+;
		       //#pragma link C++ function operator<< (std::ostream &out, const myTime &CmyTime);

//#pragma link C++ global gROOT;
//#pragma link C++ global gEnv;

#endif
