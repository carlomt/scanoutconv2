CC      = g++ -Wall  -fPIC
ROOT    = `root-config --glibs --cflags`

#ARCH          = linux
CXX           =
ObjSuf        = o
SrcSuf        = cpp
ExeSuf        =
DllSuf        = so
OutPutOpt     = -o

ROOTCFLAGS   := $(shell root-config --cflags)
ROOTLIBS     := $(shell root-config --libs)
ROOTGLIBS    := $(shell root-config --glibs)
ROOTLDFLAGS  := $(shell root-config --ldflags)

# Linux with egcs, gcc 2.9x, gcc 3.x (>= RedHat 5.2)
CXX           = g++ 
CXXFLAGS      = -O -Wall -fPIC -DWITHROOTINTERFACE  # -lboost_date_time -DDEBUGLIB
LD            = g++ 
LDFLAGS       = -O -Wall -fPIC $(ROOTLDFLAGS) # -lboost_date_time
SOFLAGS       = -shared -dynamiclib -single_module 

CXXFLAGS     += $(ROOTCFLAGS)
LIBS          = $(ROOTLIBS) $(SYSLIBS)
GLIBS         = $(ROOTGLIBS) $(SYSLIBS)




default: FileConverter.C libmyDateTime.so
		$(CXX) $(CXXFLAGS) $(LIBS) $(GLIBS) libmyDateTime.so -D__OFNAME__='"FileConverter.x"' -o FileConverter.x FileConverter.C

myDate.o: myDate.C
		$(CXX) $(CXXFLAGS)   -c myDate.C -o myDate.o

myTime.o: myTime.C
		$(CXX) $(CXXFLAGS)   -c myTime.C -o myTime.o

myStoi.o: myStoi.C
		$(CXX) $(CXXFLAGS) -c myStoi.C -o myStoi.o

libmyDateTime.so: myDate.o myTime.o DictMyDateTimeOutput.o myStoi.o
		$(CXX) $(CXXFLAGS) $(LIBS) $(GLIBS) $(SOFLAGS) myDate.o myTime.o  myStoi.o DictMyDateTimeOutput.o  -o libmyDateTime.so

clean:		
		rm -f *.x *.a *.o *.so *.pcm *.d

# Get a string containing the ROOT version.
ROOT_VERSION=`root-config --version`

DictMyDateTimeOutput.cxx: myDate.h myTime.h myDateTimeLinkDef.h
#if [[ ${ROOT_VERSION} =~ ^6 ]]; then
		rootcling -f DictMyDateTimeOutput.cxx -rml libmyDateTime.so  -rmf myDateTime.rootmap -c myDate.h  -c myTime.h myDateTimeLinkDef.h
#else
#		rootcint -v -f DictMyDateTimeOutput.cxx -c myDate.h myTime.h myDateTimeLinkDef.h
#fi


DictMyDateTimeOutput.o: DictMyDateTimeOutput.cxx
		$(CXX) $(CXXFLAGS)   -c DictMyDateTimeOutput.cxx -o DictMyDateTimeOutput.o
