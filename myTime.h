#ifndef myTime_hpp
#define myTime_hpp

#include <iostream>

#ifdef WITHROOTINTERFACE
#include "TROOT.h"
#include "TApplication.h"
#include "TSystem.h"
#endif

class myTime
{
 public:
  myTime(){};
  myTime(const myTime& rhs){};
  virtual ~myTime(){};

  void Set(const std::string hh,const std::string mm,const std::string ss);
  void Set(const unsigned int hh,const unsigned int mm,const double ss);
  
  unsigned int Hh;
  unsigned int Mm;
  double Ss;

  std::string GetISO() const;
  std::string GetRootFormat() const;

  double GetDifferenceInMinutes(const myTime &in);
  double GetDifferenceInHours(const myTime &in);

  friend std::ostream& operator<< (std::ostream &out, const myTime &CmyTime);

#ifdef WITHROOTINTERFACE
  ClassDef(myTime,1);
#endif

};


#endif /* myTime_hpp */

