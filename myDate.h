#ifndef myDate_hpp
#define myDate_hpp

#include <iostream>

#ifdef WITHROOTINTERFACE
#include "TROOT.h"
#include "TApplication.h"
#include "TSystem.h"
#endif

class myDate
{
 public:
    myDate(){};
    virtual ~myDate(){};
    myDate(const myDate& rhs){};
  myDate(unsigned int day, unsigned int month=1, unsigned int year=1970){};

  void Set(std::string day, std::string month, std::string year);
  void Set(unsigned int day, unsigned int month, unsigned int year);
  
  unsigned int Day;
  unsigned int Month;
  unsigned int Year;

  std::string GetISOExtendedFormat() const;

  long long int GetDifferenceInMinutes(const myDate &in);
  long long int GetDifferenceInHours(const myDate &in);

  //friend std::ostream& operator<< (std::ostream &out, const myDate &CmyDate);

#ifdef WITHROOTINTERFACE
  ClassDef(myDate, 1);
#endif
};

/* std::ostream& operator<< (std::ostream &out, const myDate &CmyDate) */
/* { */
/*   //  out <<CmyDate.Day<<"/"<<CmyDate.Month<<"/"<<CmyDate.Year; */
/*   out << CmyDate.GetISOExtendedFormat(); */
/*   return out; */
/* } */

#endif /* myDate_hpp */

